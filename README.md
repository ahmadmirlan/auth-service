# **Auth Service**

### **Description**

Auth service is service to handle any kind of authentication product. This service based on Node Js project with plain javascript es6 template with Express JS as a framework. This project also using serverless as a deployment template.

### **How To Run The Project**

1. please run `npm install` on root folder project
2. to run offline please use `sls offline` it will run on port 3000 http://localhost:3000. Please make sure you already have [Serverless](https://www.serverless.com/framework/docs/getting-started/) installed on your local machine.

### **Branch and Push Changes**

- Main branch it should `master` branch (no direct push into `master` branch, every change should be from `develop` branch)
- Please create your own branch and create PR to `develop` branch every change you want to merge.
- Commit messages should include the ticket number and related name with the bug or feature
