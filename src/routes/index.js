import {auth, canDeleteAdmin, canEditAdmin, canReadAdmin} from '../config/auth';
import * as roleController from '../controller/RoleController';
import * as permissionController from '../controller/PermissionController';
import * as authController from '../controller/AuthController';
import * as userController from '../controller/UserController';

export const routes = (app) => {
    // Render home page
    app.get('/home', function (req, res) {
        return res.send({message: 'Welcome to Auth Service Gateway'});
    });

    app.get('/version', (req, res) => {
        return res.send({
            service: 'Auth Service Gate Way',
            version: 'v0.0.1'
        });
    });

    app.post('/role', [auth, canEditAdmin], roleController.createNewRole);
    app.delete('/role/:id', [auth, canDeleteAdmin], roleController.deleteRoleById);
    app.put('/role/:id', [auth, canEditAdmin], roleController.updateRoleById);
    app.get('/role/:id', [auth], roleController.getRoleById);
    app.post('/roles', [auth, canReadAdmin], roleController.findRoles);
    app.get('/roles/all', [auth], roleController.getAllRoles);

    app.post('/permission', [auth, canEditAdmin], permissionController.createNewPermission);
    app.get('/permission/:id', [auth], permissionController.getPermissionById);
    app.delete('/permission/:id', [auth, canDeleteAdmin], permissionController.deletePermissionById);
    app.put('/permission/:id', [auth, canEditAdmin], permissionController.updatePermission);
    app.get('/permissions/all', [auth], permissionController.getAllPermissions);
    app.post('/permissions', [auth, canReadAdmin], permissionController.findPermissions);
    app.get('/permissions/parent', [auth, canReadAdmin], permissionController.findAllParentPermission);

    app.post('/auth/register', authController.register);
    app.post('/auth/login', authController.login);
    app.get('/auth/check/email/:email', authController.isEmailExist);
    app.get('/auth/check/username/:username', authController.isUsernameExist);

    app.post('/users', [auth, canReadAdmin], userController.findAllUsers);
    app.get('/user/myInfo', [auth], userController.findUserInfo);
    app.get('/user/:id', [auth, canReadAdmin], userController.findUserById);
    app.post('/user/add', [auth, canEditAdmin], userController.createNewUser);
    app.put('/user/:id', [auth, canEditAdmin], userController.updateUser);
};
