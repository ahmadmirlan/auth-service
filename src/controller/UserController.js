import QueryBuilder from "../config/QueryBuilder";
import {pick, pull} from 'lodash';
import {User} from "../models/userModel";
import bcrypt from "bcryptjs";
import {Role} from "../models/roleModel";

/*
* POST
* Find all users
* */
export const findAllUsers = async (req, res) => {
    let query = QueryBuilder(pick(req.body, [
        'filter',
        'pageNumber',
        'pageSize',
        'sortField',
        'sortOrder',
    ]));
    try {
        const totalElements = await User.countDocuments(query.filter);
        const users = await User.find(query.filter).limit(query.pageSize).skip(query.pageNumber * query.pageSize)
            .sort([[query.sortField, query.sortOrder]]).select('-password -activationToken -accessToken').populate('role');
        return res.sendData(users, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(500).sendError('Failed load users data');
    }
};

/*
* GET
* Get user by id
* */
export const findUserById = async (req, res) => {
    const {id} = req.params;

    try {
        const user = await User.findById(id).populate('role');
        if (!user) {
            return res.status(404).sendError('User not found!');
        }
        return res.send(user);
    } catch (e) {
        return res.status(500).sendError('Failed find user');
    }
};

/*
* GET
* Find user info
* */
export const findUserInfo = async (req, res) => {
    const {id} = req.user;

    try {
        const user = await User.findById(id).select('-refreshToken -activationToken -password').populate('role');

        if (!user) {
            return res.status(400).sendError('Please re login');
        }
        return res.send(user);
    } catch (e) {
        return res.status(500).sendError('Failed load user info');
    }
};

/*
* POST
* Create new user
* */
export const createNewUser = async (req, res) => {
    let body = req.body;

    try {
        //Validate user
        let isEmailExist = await User.findOne({email: body.email});
        let isUsernameExist = await User.findOne({username: body.username});
        if (isEmailExist) return res.status(400).send({messages: 'Email already used!'});
        if (isUsernameExist) {
            return res.status(400).send({'messages': 'Username already taken!'});
        }

        let user = new User(
            pick(body, ['email', 'role', 'username', 'firstName', 'lastName']),
        );

        /*
        * Check role
        * */
        const role = await Role.findById(user.role);


        // Set Default Avatar For User Profile
        user.pic = 'https://via.placeholder.com/300';

        // Encrypt The Password Using bcrypt
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash('12345678', salt);

        user.activationToken = user.generateAuthToken(user);
        user.status = 'INACTIVE';

        // Save User To Database
        let newUser = await user.save();
        newUser.role = role;
        newUser = pull(newUser, 'password', 'accessToken', 'refreshToken', 'activationToken');

        return res.send(newUser);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* PUT
* Update user
* */
export const updateUser = async (req, res) => {
    const uer = pick(req.body, ['firstName', 'lastName', 'username', 'email', 'role', 'status']);
    const {id} = req.params;
    try {
        const user = await User.findByIdAndUpdate(id, {...uer}, {new: true}).populate('role');
        return res.send(pull(user, 'password', 'accessToken', 'refreshToken', 'activationToken'));
    } catch (e) {
        return res.status(500).sendError('Cannot update user');
    }
};
