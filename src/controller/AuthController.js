import {User} from '../models/userModel';
import bcrypt from 'bcryptjs';
import {pick} from 'lodash';

/*
 * POST
 * Register new user
 * */
export const register = async (req, res) => {
    let body = req.body;

    //Validate user
    let isEmailExist = await User.findOne({email: body.email});
    let isUsernameExist = await User.findOne({username: body.username});
    if (isEmailExist) return res.status(400).send({messages: 'Email already used!'});
    if (isUsernameExist) {
        return res.status(400).send({'messages': 'Username already taken!'});
    }

    let user = new User(
        pick(body, ['email', 'password', 'username', 'firstName', 'lastName']),
    );

    // Set Default Avatar For User Profile
    user.pic = 'https://via.placeholder.com/300';

    //Encrypt The Password Using bcrypt
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);

    user.activationToken = user.generateAuthToken(user);

    //Save User To Database
    let newUser = await user.save();

    newUser = pick(newUser, ['firstName', 'lastName', 'email', 'username']);

    return res.send(newUser);
};

/*
* POST
* Login user
* */
export const login = async (req, res) => {
    const {email, password} = req.body;

    if (!email || !password) {
        return res.status(400).sendError('Invalid username/email or password');
    }

    try {
        const user = await User.findOne({
            $or: [{username: email}, {email: email}],
            status: ['PENDING', 'ACTIVE']
        }).populate('role');

        if (!user) {
            return res.status(400).sendError('Invalid username/email or password');
        }

        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if (!validPassword)
            return res.status(400).sendError('Invalid username/email or password!');

        user.accessToken = user.generateAuthToken(user);

        return res.send(user);

        /*user.save()
            .then(resp => {
                delete resp.password;
                delete resp.activationToken;
                res.send({user: resp});
            })
            .catch(err => {
                return res.status(500).sendError(err);
            });*/

    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* GET
* Check Email
* */
export const isEmailExist = async (req, res) => {
    const {email} = req.params;
    try {
        const user = await User.findOne({email});
        if (user) {
            return res.send({status: true});
        } else {
            return res.send({status: false});
        }
    } catch (e) {
        return res.status(500).sendError('Checking email failed');
    }
};

/*
* GET
* Check Username
* */
export const isUsernameExist = async (req, res) => {
    const {username} = req.params;
    try {
        const user = await User.findOne({username});
        if (user) {
            return res.send({status: true});
        } else {
            return res.send({status: false});
        }
    } catch (e) {
        return res.status(500).sendError('Checking username failed');
    }
};
