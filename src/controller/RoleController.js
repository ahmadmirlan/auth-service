import {pick} from 'lodash';
import {Role} from '../models/roleModel';
import QueryBuilder from "../config/QueryBuilder";

/*
 * POST
 * Create New Role
 * */
export const createNewRole = async (req, res) => {
    let body = req.body;
    let role = new Role(pick(body, ['title', 'isCoreRole', 'permissions']));
    role
        .save()
        .then(data => {
            return res.send(data);
        })
        .catch(err => {
            return res.status(400).sendError(err);
        });
};

/*
 * GET
 * Get All Roles
 * */
export const getAllRoles = async (req, res) => {
    Role.find()
        .then(data => {
            return res.send(data);
        })
        .catch(err => {
            return res.status(500).sendError(err);
        });
};

/*
 * GET
 * Get Role By Id
 * */
export const getRoleById = async (req, res) => {
    let {id} = req.params;
    Role.findById(id)
        .then(data => {
            if (data) return res.send(data);
            else
                return res.status(404).sendError(`Role with id ${id} not found!`);
        })
        .catch(err => {
            return res.status(500).sendError(err);
        });
};

/*
 * DELETE
 * Delete Role By Id
 * */
export const deleteRoleById = async (req, res) => {
    let roleId = req.params['id'];
    Role.findByIdAndDelete(roleId)
        .then(data => {
            if (data) {
                return res.send({
                    message: `Role with id ${roleId} deleted!`,
                });
            }
            return res.status(404).sendError(`Role with id ${roleId} not found!`);
        })
        .catch(err => {
            res.status(400).sendError(err);
        });
};

/*
 * UPDATE
 * Update Role By Id
 * */
export const updateRoleById = async (req, res) => {
    let roleId = req.params['id'];
    Role.findByIdAndUpdate(roleId, req.body, {new: true})
        .then(data => {
            if (data) return res.send(data);
            else return res.status(404).sendError(`Role with id ${roleId} not found`);
        })
        .catch(err => {
            return res.status(404).sendError(err);
        });
};

/*
 * POST
 * Find Roles
 * */
export const findRoles = async (req, res) => {
    try {
        let query = QueryBuilder(
            pick(req.body, [
                'filter',
                'pageNumber',
                'pageSize',
                'sortField',
                'sortOrder',
            ]),
        );
        let totalElements = await Role.countDocuments(query.filter);
        let roles = await Role.find(query.filter)
            .limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]]);
        return res.sendData(roles, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(500).sendError('Failed load roles data');
    }
};
