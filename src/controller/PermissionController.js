import {pick, remove} from 'lodash';
import {Permission} from '../models/permissionModel';
import {Role} from '../models/roleModel';
import QueryBuilder from "../config/QueryBuilder";

/*
 * POST
 * Create New Permission
 * */
export const createNewPermission = async (req, res) => {
    try {
        let body = req.body;
        let permission = new Permission(
            pick(body, ['name', 'level', 'parentId', 'title']),
        );
        let parentId = null;
        if (permission.level === 2) {
            parentId = await Permission.findById(permission.parentId);
            if (!parentId) {
                return res.status(400).sendError('Invalid parentId');
            }
        }
        permission = await permission.save();
        permission.parentId = parentId;
        /*
        * Find core role
        * Core role should be named {title: Administrator, isCoreRole: true}
        * */
        let coreRole = await Role.findOne({
            isCoreRole: true,
        });
        if (permission && coreRole) {
            coreRole.permissions.push(permission._id);
            await coreRole.save();
        }
        return res.send(permission);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
 * GET
 * Get Permission By Id
 * */
export const getPermissionById = async (req, res) => {
    let permissionId = req.params['permissionId'];
    Permission.findById(permissionId)
        .populate('parentId')
        .then(data => {
            if (data) return res.send(data);
            else
                return res
                    .status(404)
                    .sendError(`Permission with id ${permissionId} not found!`);
        })
        .catch(err => {
            return res.status(500).sendError(err);
        });
};

/*
 * GET
 * Find All Permission
 * */
export const getAllPermissions = async (req, res) => {
    try {
        const permissions = await Permission.find();
        return res.send(permissions);
    } catch (e) {
        return res.status(500).sendError('Failed load permissions data');
    }
};

/*
 * DELETE
 * Delete Permission By Id
 * */
export const deletePermissionById = async (req, res) => {
    const {id} = req.params;
    try {
        let permission = await Permission.findOne({_id: id, isCorePermission: false});

        if (!permission) {
            return res.status(400).sendError('permission not found');
        }

        permission = await permission.delete();
        let roles = await Role.find();
        if (permission) {
            for (let role of roles) {
                remove(role.permissions, _perm => {
                    return _perm.toString() === id.toString();
                });
                await role.save();
            }
        }
        return res.send({message: `Permission with id ${id} deleted!`});
    } catch (e) {
        res.status(400).sendError(e);
    }
};

/*
* POST
* Find all permissions
* */
export const findPermissions = async (req, res) => {
    let query = QueryBuilder(pick(req.body, [
        'filter',
        'pageNumber',
        'pageSize',
        'sortField',
        'sortOrder',
    ]));
    try {
        const totalElements = await Permission.countDocuments(query.filter);
        const permissions = await Permission.find(query.filter).limit(query.pageSize).skip(query.pageNumber * query.pageSize)
            .sort([[query.sortField, query.sortOrder]]).populate('parentId');
        return res.sendData(permissions, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(500).sendError('Failed load permissions');
    }
};

/*
* PUT
* Update permission
* */
export const updatePermission = async (req, res) => {
    const {id} = req.params;
    try {
        const permission = await Permission.findOneAndUpdate({
            _id: id,
            isCorePermission: false
        }, pick(req.body, ['name', 'level', 'parentId', 'title']), {new: true})
            .populate('parentId');
        return res.send(permission);
    } catch (e) {
        return res.status(500).sendError('Failed updating permissions');
    }
};

/*
* GET
* Find all parent (level 1) permission
* */
export const findAllParentPermission = async (req, res) => {
    try {
        const permission = await Permission.find({level: 1});
        return res.send(permission);
    } catch (e) {
        return res.status(500).sendError('Failed load parent permission');
    }
};
