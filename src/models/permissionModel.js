import {Schema, model, ObjectId} from 'mongoose';
import toJson from '@meanie/mongoose-to-json';

const permissionSchema = new Schema({
        name: {
            type: String,
            required: true,
            minLength: 2,
        },
        level: {
            type: Number,
            required: true,
        },
        title: {
            type: String,
        },
        parentId: {
            type: ObjectId,
            ref: 'Permission',
        },
        isCorePermission: {
            type: Boolean,
            required: true,
            default: false
        }
    },
    {
        timestamps: true,
        toJSON: {getters: true},
        toObject: {getters: true},
    });

permissionSchema.plugin(toJson);
export const Permission = model('Permission', permissionSchema);
