import {Schema, model, ObjectId} from 'mongoose';
import toJson from '@meanie/mongoose-to-json';
import jwt from 'jsonwebtoken';

const userSchema = new Schema({
    firstName: {
        type: String,
        required: true,
        minlength: 1
    },
    lastName: {
        type: String,
        minlength: 1
    },
    username: {
        type: String,
        unique: true,
        minlength: 6,
        maxlength: 12,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true,
    },
    accessToken: {type: String},
    refreshToken: {type: String},
    activationToken: {type: String},
    pic: {
        type: String,
    },
    role: {
        type: ObjectId,
        ref: 'Role',
    },
    status: {
        type: String,
        enum: ['ACTIVE', 'BLOCKED', 'DELETED', 'PENDING', 'INACTIVE'],
        default: 'PENDING'
    },
    isCoreUser: {
        type: Boolean,
        required: true,
        default: false
    }
});

/*Generated Token*/
userSchema.methods.generateAuthToken = user => {
    return jwt.sign(
        {id: user._id, _id: user._id, username: user.username, email: user.email, role: user.role ? user.role.id : null},
        process.env.JWT_PRIVATE_KEY,
        {
            expiresIn: 86400,
        },
    );
};

userSchema.plugin(toJson);
export const User = model('User', userSchema);
