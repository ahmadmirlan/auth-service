import {Schema, model, ObjectId} from 'mongoose';

const toJson = require('@meanie/mongoose-to-json');

const roleSchema = new Schema(
    {
        title: {
            type: String,
        },
        isCoreRole: {
            type: Boolean,
            required: true,
            default: false
        },
        permissions: [
            {
                type: ObjectId,
                ref: 'Permission',
            },
        ],
    },
    {
        timestamps: true,
        toJSON: {getters: true},
        toObject: {getters: true},
    },
);

roleSchema.plugin(toJson);
export const Role = model('Role', roleSchema);
