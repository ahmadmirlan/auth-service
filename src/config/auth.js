import jwt from 'jsonwebtoken';
import {Role} from "../models/roleModel";

export function auth(req, res, next) {
    let token = req.header('Authorization');

    if (!token)
        return res.status(401).sendError('Access denied. No token provided.');

    token = token.replace('Bearer ', '');

    if (token.includes('Bearer')) {
        token = token.split(' ')[1];
    }

    try {
        req.user = jwt.verify(token, process.env.JWT_PRIVATE_KEY);
        next();
    } catch (ex) {
        res.status(401).sendError(' Token Invalid or Expired.');
    }
}

export async function canReadAdmin(req, res, next) {
    try {
        const role = await checkRoleHasAccess(req.user.role, process.env.CAN_READ_ADMIN);
        if (role) {
            next();
        } else {
            return res.status(403).sendError('Access denied. user has no permission access');
        }
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export async function canEditAdmin(req, res, next) {
    try {
        const role = await checkRoleHasAccess(req.user.role, process.env.CAN_EDIT_ADMIN);
        if (role) {
            next();
        } else {
            return res.status(403).sendError('Access denied. user has no permission access');
        }
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export async function canDeleteAdmin(req, res, next) {
    try {
        const role = await checkRoleHasAccess(req.user.role, process.env.CAN_DELETE_ADMIN);
        if (role) {
            next();
        } else {
            return res.status(403).sendError('Access denied. user has no permission access');
        }
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

async function checkRoleHasAccess(roleId, permissionId) {
    try {
        const role = await Role.findById(roleId);
        if (!role || !role.permissions) {
            return false;
        }

        const canReadAdmin = role.permissions.find(perm => perm.toString() === permissionId.toString());

        return !!canReadAdmin;
    } catch (e) {
        return false;
    }
}
