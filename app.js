import {connectDatabase} from './src/config/db';
import {RestManager} from './src/config/RestBuilder';

const cors = require('cors');
const serverless = require('serverless-http');
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const router = require('./src/routes');
const app = express();

connectDatabase();

app.use(cors());
app.use(cookieParser());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));
// parse application/json
app.use(bodyParser.json());

app.use(RestManager);

// Routes
router.routes(app);

export const handler = serverless(app);
